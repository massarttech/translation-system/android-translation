package com.massarttech.android.translation.domain;

import java.io.Serializable;


public class SaveRequest implements Serializable {
    private final String key;
    private final String text;
    private final String screenName;

    public SaveRequest(String key, String text, String screenName) {
        this.key = key;
        this.text = text;
        this.screenName = screenName;
    }

    public String getKey() {
        return key;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getText() {
        return text;
    }
}
