package com.massarttech.android.translation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.massarttech.android.translation.backend.ApiHandler;
import com.massarttech.android.translation.backend.OnApiResponse;
import com.massarttech.android.translation.domain.Translation;
import com.massarttech.android.translation.internal.ArabicTranslator;
import com.massarttech.android.translation.internal.EnglishTranslator;
import com.massarttech.android.translation.internal.Translator;
import com.massarttech.android.translation.internal.TurkishTranslator;
import com.massarttech.android.translation.internal.UrduTranslator;
import com.massarttech.android.translation.internal.data.DataHolder;

import java.util.HashMap;

public final class TranslationHelper {
    @SuppressLint("StaticFieldLeak")
    private static TranslationHelper instance;
    private static Translator translator;
    private final TranslationOptions options;
    private Context context;
    private DataHolder dataHolder;

    private TranslationHelper(@NonNull TranslationOptions options) {
        this.options = options;
    }

    public static void initialize(@NonNull Context context, @NonNull TranslationOptions options) {
        if (instance == null) {
            DataHolder dataHolder = DataHolder
                    .getInstance(ApiHandler.getInstance(options.getBaseUrl()), context);
            initializeTranslator(options.getCurrentLanguage(), dataHolder);
            instance = new TranslationHelper(options);
            instance.dataHolder = dataHolder;
            instance.context = context;
        } else {
            initializeTranslator(options.getCurrentLanguage(), instance.dataHolder);
        }
    }

    public static void updateLanguage(@NonNull Language language) {
        if (instance == null) {
            throw new IllegalArgumentException("not initialized");
        }
        instance.options.setCurrentLanguage(language);
        initializeTranslator(language, instance.dataHolder);
    }

    private static void initializeTranslator(@NonNull Language language,
                                             @NonNull DataHolder dataHolder) {
        switch (language) {
            case ARABIC:
                translator = new ArabicTranslator(dataHolder);
                break;
            case ENGLISH:
                translator = new EnglishTranslator(dataHolder);
                break;
            case URDU:
                translator = new UrduTranslator(dataHolder);
                break;
            case TURKISH:
                translator = new TurkishTranslator(dataHolder);
                break;
            default:
                throw new IllegalArgumentException("TranslationHelper does not support " + language.name());
        }
    }

    public static void loadTranslations(@NonNull final Context context, @NonNull final TranslationLoadListener listener) {
        final HashMap<String, HashMap<String, Translation>> tempMap = instance.dataHolder.getMap(context);
        if (tempMap != null && !tempMap.isEmpty()) {
            instance.dataHolder.setMap(tempMap);
            listener.onTranslationsLoaded();
        }
        ApiHandler.getInstance(instance.options.getBaseUrl())
                .loadAppData(new OnApiResponse<HashMap<String, HashMap<String, Translation>>>() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onSuccess(@NonNull HashMap<String, HashMap<String, Translation>> response) {
                        instance.dataHolder.setTranslations(context, response);
                        if (tempMap!= null && tempMap.isEmpty()) listener.onTranslationsLoaded();
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    public static void doTranslations(@NonNull Activity activity, @NonNull String screenName) {
        checkPreConditions(activity.getWindow().getDecorView().getRootView());
        doTranslations((ViewGroup) activity.getWindow()
                .getDecorView().findViewById(android.R.id.content), screenName);
    }

    private static void checkPreConditions(View view) {
        if (instance == null) {
            throw new IllegalArgumentException("You forgot to call initialize in your App class.");
        }
        if (!(view instanceof ViewGroup)) {
            throw new NullPointerException("View must be ViewGroup.");
        }
    }

    public static void doTranslations(@NonNull ViewGroup rootView, @NonNull String screenName) {
        checkPreConditions(rootView);
        translator.doTranslation(rootView, screenName);
    }

    @NonNull
    public static String getTranslation(@NonNull String screenName, @NonNull String key, Object... formatArgs) {
        return translator.getTranslation(instance.context, screenName, key, formatArgs);
    }

    public static void doTranslations(View view, @NonNull String screenName) {
        if (view instanceof ViewGroup) {
            doTranslations((ViewGroup) view, screenName);
        }
    }

    @NonNull
    public static Language currentLanguage() {
        return instance.options.getCurrentLanguage();
    }

    @FunctionalInterface
    public interface TranslationLoadListener {
        void onTranslationsLoaded();
    }
}
