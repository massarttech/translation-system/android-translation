package com.massarttech.android.translation.internal.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.massarttech.android.translation.backend.ApiHandler;
import com.massarttech.android.translation.domain.SaveRequest;
import com.massarttech.android.translation.domain.Translation;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public final class DataHolder {
    private static DataHolder instance;
    private static final String TAG = DataHolder.class.getSimpleName();
    private HashMap<String, HashMap<String, Translation>> screenMap;
    private final ApiHandler apiHandler;
    private final Context appContext;

    private DataHolder(ApiHandler apiHandler, Context appContext) {
        this.apiHandler = apiHandler;
        this.appContext = appContext;
    }

    public static DataHolder getInstance(@NonNull ApiHandler apiHandler,
                                         @NonNull Context context) {
        if (instance == null) {
            instance = new DataHolder(apiHandler, context);
        }
        return instance;
    }


    @Nullable
    public Translation getTranslation(@NonNull String screenName,
                                      @NonNull String key) {
        if (screenMap == null) {
            return null;
        }
        Map<String, Translation> translationMap = screenMap.get(screenName);
        if (translationMap == null) {
            Log.e(TAG, "getTranslation: no translations found for screen name " + screenName);
            return null;
        }
        Translation translation = translationMap.get(key);
        if (translation == null) {
            Log.e(TAG, "getTranslation: no translation found for key " + key + " in screen " + screenName);
            saveMissedTranslation(screenName, key, translationMap);
        }
        return translation;
    }

    private void saveMissedTranslation(@NonNull String screen,
                                       @NonNull String key,
                                       @NonNull Map<String, Translation> map) {
        int identifier = appContext.getResources()
                .getIdentifier(key, "string", appContext.getPackageName());
        try {
            String original = appContext.getString(identifier);
            if (original != null) {
                Log.i(TAG, "saveMissedTranslation:" +
                        " saving missed translation for key " + key + " in screen " + screen);
                apiHandler.save(new SaveRequest(key, original, screen));
                Translation translation = new Translation();
                translation.setOriginal(original);
                translation.setKey(key);
                translation.setEnglish(original);
                map.put(key, translation);
            }
        } catch (Exception e) {
            //can be any format exception

        }
    }

    public void setTranslations(Context context, @NonNull HashMap<String, HashMap<String, Translation>> screenMap) {
        this.screenMap = screenMap;
        saveMap(context, screenMap);
    }

    public void setMap(@NonNull HashMap<String, HashMap<String, Translation>> screenMap) {
        this.screenMap = screenMap;
    }


    private void saveMap(Context context, HashMap<String, HashMap<String, Translation>> screenMap) {
        SharedPreferences pSharedPref = context.getApplicationContext().getSharedPreferences(
                context.getPackageName() + "TRANSLATIONS",
                Context.MODE_PRIVATE
        );
        if (pSharedPref != null) {
            String jsonString = new Gson().toJson(screenMap);
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.remove("TRANSLATIONS_MAP").apply();
            editor.putString("TRANSLATIONS_MAP", jsonString);
            editor.apply();
        }
    }

    public HashMap<String, HashMap<String, Translation>> getMap(Context context) {
        SharedPreferences pSharedPref = context.getApplicationContext().getSharedPreferences(
                context.getPackageName() + "TRANSLATIONS", Context.MODE_PRIVATE);
        try {
            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString("TRANSLATIONS_MAP", (new JSONObject()).toString());
                return new Gson().fromJson(jsonString, new TypeToken<HashMap<String, HashMap<String, Translation>>>() {
                }.getType());
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

}
