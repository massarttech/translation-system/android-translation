package com.massarttech.android.translation.internal;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

import com.massarttech.android.translation.internal.data.DataHolder;

@RestrictTo(RestrictTo.Scope.LIBRARY)
public abstract class Translator {
    protected final DataHolder dataHolder;

    protected Translator(DataHolder dataHolder) {
        this.dataHolder = dataHolder;
    }

    public final void doTranslation(@NonNull ViewGroup view, @NonNull String screenName) {
        for (int i = 0; i < view.getChildCount(); i++) {
            View currentChild = view.getChildAt(i);
            if (currentChild instanceof ViewGroup) {
                doTranslation((ViewGroup) currentChild, screenName);
            }
            if (currentChild instanceof EditText) {
                CharSequence hint = ((EditText) currentChild).getHint();
                if (!TextUtils.isEmpty(hint)) {
                    Object tag = currentChild.getTag();
                    if (tag instanceof String) {
                        String key = (String) tag;
                        ((EditText) currentChild).setHint(getTranslation(view.getContext(), screenName, key));
                    }
                }
            } else if (currentChild instanceof TextView) {
                CharSequence text = ((TextView) currentChild).getHint();
                if (text == null || text.length() < 1) {
                    text = ((TextView) currentChild).getText();
                    if (!TextUtils.isEmpty(text)) {
                        Object tag = currentChild.getTag();
                        if (tag instanceof String) {
                            String key = (String) tag;
                            ((TextView) currentChild).setText(getTranslation(view.getContext(), screenName, key));
                        }
                    }
                } else {
                    if (!TextUtils.isEmpty(text)) {
                        Object tag = currentChild.getTag();
                        if (tag instanceof String) {
                            String key = (String) tag;
                            ((TextView) currentChild).setHint(getTranslation(view.getContext(), screenName, key));
                        }
                    }
                }
            }
        }

    }


    @NonNull
    public String getTranslation(@NonNull Context context, @NonNull String screenName, @NonNull String key, Object... params) {
        try {
            int identifier = context.getResources()
                    .getIdentifier(key, "string", context.getPackageName());
            if (params != null && params.length > 0) {
                try {
                    return context.getString(identifier, params);
                } catch (Exception e) {
                    //can be any format exception
                    return context.getString(identifier);
                }
            }
            return context.getString(identifier);
        } catch (Resources.NotFoundException e) {
            return key;
        }
    }


}
