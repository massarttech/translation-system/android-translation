package com.massarttech.android.translation;

public enum Language {
    ENGLISH("en"),
    ARABIC("ar"),
    URDU("ur"),
    TURKISH("tr");

    private final String langCode;

    Language(String languageCode) {
        this.langCode = languageCode;
    }

    public String getLangCode() {
        return langCode;
    }

    public static Language fromString(String langCode) {
        for (Language b : Language.values()) {
            if (b.langCode.equalsIgnoreCase(langCode)) {
                return b;
            }
        }
        throw new IllegalArgumentException("No enum found for the constant " + langCode);
    }
}
