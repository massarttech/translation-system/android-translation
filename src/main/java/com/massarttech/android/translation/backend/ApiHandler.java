package com.massarttech.android.translation.backend;

import android.util.Log;

import androidx.annotation.NonNull;

import com.massarttech.android.translation.domain.JunkResponse;
import com.massarttech.android.translation.domain.SaveRequest;
import com.massarttech.android.translation.domain.Translation;

import java.util.HashMap;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiHandler {
    private static ApiHandler instance;
    private final EndPoints endPoints;
    private static final String TAG = ApiHandler.class.getSimpleName();

    private ApiHandler(@NonNull String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        endPoints = retrofit.create(EndPoints.class);
    }

    public static ApiHandler getInstance(@NonNull String baseUrl) {
        if (instance == null) {
            instance = new ApiHandler(baseUrl);
        }
        return instance;
    }

    public void save(@NonNull SaveRequest request) {
        endPoints.addTranslation(request)
                .enqueue(new Callback<>(new OnApiResponse<JunkResponse>() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onSuccess(@NonNull JunkResponse response) {
                        Log.i(TAG, "onSuccess: missed translation key added.");
                    }

                    @Override
                    public void onError() {
                        Log.e(TAG, "onError: failed to send missed key to server");
                    }
                }));
    }


    public void loadAppData(@NonNull OnApiResponse<HashMap<String, HashMap<String, Translation>>> completion) {
        endPoints.getTranslations().enqueue(new Callback<>(completion));
    }
}
