package com.massarttech.android.translation.backend;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by bullhead on 2/7/18.
 */

class Callback<T> implements retrofit2.Callback<T> {
    private final OnApiResponse<T> apiResponse;

    Callback(@Nullable OnApiResponse<T> apiResponse) {
        this.apiResponse = apiResponse;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (apiResponse == null) {
            return;
        }
        apiResponse.onResponse();
        T body = response.body();
        if (body != null) {
            apiResponse.onSuccess(body);
        } else {
            apiResponse.onError();
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if (apiResponse == null) {
            return;
        }
        apiResponse.onResponse();
        apiResponse.onError();

    }
}
