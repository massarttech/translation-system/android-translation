package com.massarttech.android.translation.backend;

import com.massarttech.android.translation.domain.JunkResponse;
import com.massarttech.android.translation.domain.SaveRequest;
import com.massarttech.android.translation.domain.Translation;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface EndPoints {
    @GET("translations/get")
    Call<HashMap<String, HashMap<String, Translation>>> getTranslations();

    @POST("translations/add")
    Call<JunkResponse> addTranslation(@Body SaveRequest request);
}
